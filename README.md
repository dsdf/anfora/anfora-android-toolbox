This project is a command line tool for Android used by AnForA.

#Available commands

## Get time
```bash
./anfora-android-toolbox-x86_64 time get
```

## Set time
The time to be set is a UNIX timestamp SECONDS.USEC, max precision is 6 digit after comma

```bash
./anfora-android-toolbox-x86_64 time set 1700903420.123456
```

## Find
Get a list of every file and folder in a directory

```bash
./anfora-android-toolbox-x86_64 find /data
```

## Recursive find
Get a list of every file and folder in a directory recursively

```bash
./anfora-android-toolbox-x86_64 findrec /data
```


