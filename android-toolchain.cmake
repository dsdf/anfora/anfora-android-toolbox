# https://stackoverflow.com/a/67729248
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)
set(CMAKE_CXX_STANDARD 20)

if(DEFINED ENV{NDK_HOME})
    set(CMAKE_TOOLCHAIN_DIRECTORY "$ENV{NDK_HOME}/build/cmake")
endif()

if(NOT DEFINED ENV{NDK_HOME})
    set(CMAKE_TOOLCHAIN_DIRECTORY "$ENV{ANDROID_HOME}/ndk/$ENV{NDK_VERSION}/build/cmake")
endif()

if(NOT CMAKE_TOOLCHAIN_FILE)
    if(NOT ANDROID_ABI)
        set(ANDROID_ABI "x86")
    endif()
    if(NOT ANDROID_PLATFORM)
        set(ANDROID_PLATFORM "android-25")
    endif()
    set(CMAKE_TOOLCHAIN_FILE "${CMAKE_TOOLCHAIN_DIRECTORY}/android.toolchain.cmake")
endif()

#if(WIN32 AND NOT CMAKE_MAKE_PROGRAM)
#    set(CMAKE_MAKE_PROGRAM "$ENV{ANDROID_HOME}/ndk-bundle/prebuilt/windows/bin/make.exe" CACHE INTERNAL "" FORCE)
#endif()