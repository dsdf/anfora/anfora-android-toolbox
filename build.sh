#!/bin/sh

# https://developer.android.com/ndk/guides/cmake#android_abi
TARGETS="armeabi-v7a arm64-v8a x86 x86_64"

BUILD_PATH="build"
DIST_PATH="dist"
BUILD_TYPE="Release" # Release Debug
ANDROID_PLATFORM="android-25"

mkdir -p "${DIST_PATH}"

# https://stackoverflow.com/a/47148032
for TARGET in ${TARGETS}
do
    # create one build dir per target architecture
    mkdir -p "${BUILD_PATH}/${TARGET}"
    cd "${BUILD_PATH}/${TARGET}" || exit

    cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE="${BUILD_TYPE}" -DANDROID_PLATFORM="${ANDROID_PLATFORM}" -DANDROID_ABI="${TARGET}" ../..
    make -j4
    cp AnForAAndroidToolbox "../../${DIST_PATH}/anfora-android-toolbox-${TARGET}"

    cd - || exit
done
