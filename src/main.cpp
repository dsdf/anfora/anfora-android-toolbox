#include <iostream>
#include <sys/time.h>
#include <cstring>
#include <vector>
#include <filesystem>

std::vector<std::string> *string_split(const std::string &s, const std::string &del = " ")
{
	auto *string_list = new std::vector<std::string>;
	unsigned long start = 0;
	unsigned long end = s.find(del);
	while (end != -1)
	{
		string_list->push_back(s.substr(start, end - start));
		start = end + del.size();
		end = s.find(del, start);
	}
	string_list->push_back(s.substr(start, end - start));
	return string_list;
}

bool is_number(const std::string& str)
{
	char* ptr;
	strtol(str.c_str(), &ptr, 10);
	return *ptr == '\0';
}

int subcommand_time(int i, char *arguments[])
{
	auto operation = std::string(arguments[0]);
	if (i == 1 && operation == "get")
	{
		struct timeval timeval1{};
		struct timezone timezone1{};
		int return_value = gettimeofday(&timeval1, &timezone1);
		if (return_value == 0)
			std::cout << timeval1.tv_sec << "." << timeval1.tv_usec << std::endl;
		else
			std::cout << "ERROR " << return_value << ": " << strerror(errno) << std::endl;
		return return_value;
	} else if (i == 2 && operation == "set")
	{
		auto splitted = string_split(std::string(arguments[1]), ".");
		struct timeval toset{};
		int retvalue = -1;
		try {
			auto str_sec = (*splitted)[0];
			auto str_usec = (*splitted)[1];
			if (!is_number(str_sec) || !is_number(str_usec))
				throw std::invalid_argument("String is not number");
			toset.tv_sec = std::stoi(str_sec);
			// tv_usec max is 999999
			if(str_usec.length() > 6)
				str_usec[6] = '\0';
			toset.tv_usec = std::stoi(str_usec);

			int return_value = settimeofday(&toset, nullptr);
			if (return_value == 0)
				std::cout << "SUCCESS" << std::endl;
			else
				std::cout << "ERROR " << return_value << ": " << strerror(errno) << std::endl;
			retvalue = return_value;
		} catch(std::invalid_argument &e1) {
			std::cout << "Error parsing input" << std::endl;
		} catch(std::out_of_range &e1) {
			std::cout << "Error parsing input, out of range" << std::endl;
		}
		delete splitted;
		return retvalue;
	} else
	{
		std::cout << "subcommand_time: unknown operation" << std::endl;
	}
	return -1;
}

int print_dir_recursive(char *arguments[], bool recursive)
{
	auto path = std::string(arguments[0]);
	//TODO remove code duplication
	if (recursive)
	{
		for (std::filesystem::recursive_directory_iterator i(path), end; i != end; ++i)
			if (is_directory(i->path()))
				std::cout << i->path().generic_string() << "/\n";
			else if (is_regular_file(i->path()))
				std::cout << i->path().generic_string() << "\n";
	} else {
		for (std::filesystem::directory_iterator i(path), end; i != end; ++i)
			if (is_directory(i->path()))
				std::cout << i->path().generic_string() << "/\n";
			else if (is_regular_file(i->path()))
				std::cout << i->path().generic_string() << "\n";
	}

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		auto subtool = std::string(argv[1]);
		if (subtool == "time")
		{
			return subcommand_time(argc - 2, &argv[2]);
		} else if (subtool == "find")
		{
			return print_dir_recursive(&argv[2], false);
		} else if (subtool == "findrec")
		{
			return print_dir_recursive(&argv[2], true);
		} else {
			std::cout << "Unknown command" << std::endl;
		}
	} else {
		std::cout << "Not enough arguments" << std::endl;
	}

	return -1;
}
